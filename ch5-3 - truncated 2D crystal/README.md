# Truncated effectively 2D structure
*Resonances_2D_PBG_waveguide.meep.ctl* is based on *MEEP/2D-hexagonal-PBG-wvg/2D_PBG_waveguide.ctl*

Simulation for a truncated crystal with 6 cells:
```sh
meep-mpi  RADIUS=0.24 NUM_CELL_1=6 DPML=2.0 excite-mode?=false fcen=0.47 df=0.3  Resonances_2D_PBG_waveguide.meep.ctl | tee 2D_PBG_wvg-f-0.47-df-0.3-pml-2.0-r-0.24-cells-6.out
```
Extracting the resonances:
```sh
grep harminv0: 2D_PBG_wvg-f-0.47-df-0.3-pml-2.0-r-0.24-cells-6.out | tee -a harminv_2D-f-0.47-df-0.3-r-0.24.csv
```

The simulation was repeated for a varied number of cells, ranging from 5 to 11. The dominating resonance was chosen from each simulation based on maximal Q-factor. The data $`Q(n_{cells})`$ was patched together manually and plotted afterwards.

 To get the field pattern of a certain resonance the simulation was repeated with the resonance frequency and a narrow bandwidth. The boolean parameter `excite-mode?` controls the output of the field.
 ```sh
meep-mpi  RADIUS=0.24 NUM_CELL_1=5 DPML=2.0 excite-mode?=true fcen=0.46 df=0.01  Resonances_2D_PBG_waveguide.meep.ctl | tee 2D_PBG_wvg-f-0.46-df-0.01-pml-2.0-r-0.24-cells-5.out
```


Keep in mind that the fields are output to a separate directory.  Have a look in the script for 
```scheme
(use-output-directory dir-name)
```
 for more datails.
