; Max Kellermeier, max.kellermeier@hotmail.de, 2017
; --- Geometry parameters
; Distance between rods in x resp. y direction
(define-param DISTANCE 1)  ; corresponds to 'a'  in Cowan, B.
(define-param VERTICAL_PERIOD (* DISTANCE (sqrt 2)) ); corresponds to 'c'  in Cowan, B.

; Rod dimensions
(define-param RODWIDTH 0.333)  ; corresponds to 'w'  in Cowan, B.
(define-param RODHEIGHT (* VERTICAL_PERIOD 0.25 ) ) ; corresponds to 'c'/4  in Cowan, B.,
; Material
(define-param EPSILON (* 1.95 1.95)) 

(define-param GRID_SIZE_X DISTANCE) ;
(define-param GRID_SIZE_Y DISTANCE) ;
(define-param GRID_SIZE_Z VERTICAL_PERIOD)

(define Glass (make dielectric (epsilon EPSILON)))
; ----------------------------------------------------------------------------

(set! num-bands 6)       
(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)
                            (basis1 1 0 0)
                            (basis2 0 1 0)
                            (basis3 0 0 1)
                        )
)                       

(define geometryList (list  ) )
(set! geometryList (append geometryList
        (list
          (make block
              (center 0 0 (* -1.5 RODHEIGHT ) )
              (size RODWIDTH DISTANCE RODHEIGHT)
              (material Glass)
          )
          (make block
              (center 0 0 (* -0.5 RODHEIGHT ) )
              (size DISTANCE RODWIDTH RODHEIGHT)
              (material Glass)
          )
          ;(geometric-object-duplicates (vector3 DISTANCE 0 0) 0  1
            (make block
                (center (* -0.5 DISTANCE)  0 (* 0.5 RODHEIGHT ) )
                (size RODWIDTH DISTANCE RODHEIGHT)
                (material Glass)
            )
            (make block
                (center (* 0.5 DISTANCE)  0 (* 0.5 RODHEIGHT ) )
                (size RODWIDTH DISTANCE RODHEIGHT)
                (material Glass)
            )
          ;)
          ;(geometric-object-duplicates (vector3 0 DISTANCE 0) 0  1
            (make block
                (center 0 (* -0.5 DISTANCE)  (* 1.5 RODHEIGHT ) )
                (size DISTANCE RODWIDTH RODHEIGHT)
                (material Glass)
            )
            (make block
                (center 0 (* 0.5 DISTANCE)  (* 1.5 RODHEIGHT ) )
                (size DISTANCE RODWIDTH RODHEIGHT)
                (material Glass)
            )
          ;)

        )
    )
)                        
                        
(set! geometry geometryList )                        
                        
                                         
(define Gamma (vector3 0 0 0)) 
(define X (vector3 0.5 0 0 ) )
(define M (vector3 0.5 0.5 0) ) ; M 
(define R (vector3 0.5 0.5 0.5))
(define T (vector3 0 0.5 0.5))
(define Z (vector3 0 0 0.5))

(set! k-points (list Gamma X M R  X Gamma Z R T Z Gamma))       
(set! k-points (interpolate 10 k-points))

(set! resolution 64)


(run (output-at-kpoint R 
			  fix-efield-phase output-efield-z))

			  