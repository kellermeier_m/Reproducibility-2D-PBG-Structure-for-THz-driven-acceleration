# MPB based computation
Based on *MPB/simple-cubic/bandstructure-simple.ctl*.
The rod width was varied from 0.2 to 0.6 in steps of 0.05. 
```sh
mpb RODWIDTH=0.2 bandstructure.mpb.ctl | tee bands-woodpile-rod-cell-ratio-0.200.out
```
Extracting the bands:
```sh
grep freqs bands-woodpile-rod-cell-ratio-0.200.out > bands-woodpile-rod-cell-ratio-0.200.freq.dat
```
The jupyter notebook *Plot woodpile band diagram.ipynb* for plotting is based on *MPB/Band Data Analysis woodpile.ipynb*. The specified paths to the output data and the figure image have to be adjusted according to your system.

# MEEP based computation
Based on *MEEP/band-structure-woodpile/sim-bands.ctl*
```sh
meep RODWIDTH=0.25 bandstructure.meep.ctl  | tee meep_band_structure_w_0.25_c_1.41.out
```
```sh
grep freqs: meep_band_structure_w_0.25_c_1.41.out > meep_band_structure_w_0.25_c_1.41.freq.dat
```
Use *Woodpile Band Diagram - meep.ipynb* for plotting.

