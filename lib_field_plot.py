# Max Kellermeier, max.kellermeier@hotmail.de, 2017
import numpy as np
from mpl_toolkits import mplot3d
from matplotlib import pyplot as plt
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
from IPython.display import display
import os
import glob 
import pandas as pd
from itertools import chain

import h5py  # http://docs.h5py.org/en/latest/quick.html



def plot_ez_by_h5(fpath, dataset=None, direction='z', cutPosition=0, fig=None, axes=None):
    directory, filename=os.path.split(fpath)
    h5_file= h5py.File(fpath)
    dict_direction = {"z": 0}
    
    if not dataset is None:
        if dataset not in h5_file.keys():
            print('h5 file does not contain a dataset with name "'+dataset +'"')
            print('Valid keys:')
            for key in h5_file.keys():
                print(key)
            return None
        fielddata= h5_file.get(dataset)
    else:
        datasets=[key for key in h5_file.keys() if h5_file.get(key).ndim > 1 ]
        dataset= datasets[0]
        fielddata= h5_file.get(dataset)
    
    # look for epsilon file in directory
    try:
        eps_file=h5py.File(
            glob.glob(
                os.path.join(directory, "*eps*.h5"))[0]
        )
        boolEps=True
    except :
        print("No epsilon file found. Plotting without...")
        boolEps=False
        eps_file={}

    if 'data-new' in eps_file:
        eps_data= eps_file.get('data-new')
    elif 'data' in eps_file:
        eps_data= eps_file.get('data')
    elif 'eps' in eps_file:
        eps_data= eps_file.get('eps')
    else:
        eps_data=None
    
    if fig is None:
        fig=plt.figure(figsize=plt.figaspect(1))
    else:
        for a in fig.get_axes():
            a.clear()           
        fig.clear()
    ax=fig.add_subplot(111)
    #flag_3d=False
    if boolEps and not (eps_data is None):
        eps_plot= ax.contour(np.rot90(eps_data), 0, colors='k', alpha=0.5)
    
    shape=fielddata.shape
    if len(shape)==2:
        data_to_plot=fielddata
        cbar_max= max(np.abs(np.min(fielddata)), np.max(fielddata))
        ez_plot= ax.pcolor(np.rot90(data_to_plot), cmap='RdBu', vmax= cbar_max , vmin=-cbar_max)
        ax.set_aspect('equal')
        
    elif len(shape)==4:
        size = shape[0] if direction=='x' else shape[1] if direction=='y' else shape[2] if direction=='z' else  None
        sl_n=slice(None)
        sl_obj= [size//2, sl_n, sl_n]
        sl_t=[0]
        ez_plot=None
        def on_value(index):
            global sl_obj
            global ez_plot
            index = index + size//2
            if direction == 'x':
                sl_obj= [index, sl_n, sl_n]
            if direction == 'y':
                sl_obj= [ sl_n, index, sl_n]
            if direction == 'z':
                sl_obj= [ sl_n,  sl_n, index]
            
            with h5py.File(fpath) as h5_f:
                ax.clear()
                fielddata=h5_f.get(dataset)
                ez_plot = ax.imshow( np.rot90(fielddata[tuple(sl_obj + sl_t)])
                    , cmap='RdBu', vmax= np.max(fielddata) , vmin=-np.max(fielddata))
                fig.canvas.draw()
            cbar = fig.colorbar(ez_plot)
            cbar.ax.set_ylabel("$E_z$ [a.u.]")    
            

        
        w= widgets.IntSlider(min= -(size//2),max= size//2 if size%2 ==1 else size//2 -1 ,
                  step=1) 
        interact(on_value, index=w)
        on_value(0)
           
    ## setting ticks
    xsize=shape[0]
    ysize=shape[1]
    res=64
    n_cells=5 
    stepsize=xsize/n_cells
    
    l=np.array(np.arange(n_cells)*stepsize)
    ax.set_xticks(l)
    ax.set_xticklabels( (l - xsize/2 )/res)
       
    stepsize=ysize/n_cells
    l=np.array(np.arange(n_cells)*stepsize)
    ax.set_yticks(l)
    ax.set_yticklabels([ "{0:0.2f}".format(tick) for tick in  (l-ysize//2)/res ] )
    
    ax.set_xlabel("x/a")
    ax.set_ylabel("y/a")
    #### color bar
    cbar = fig.colorbar(ez_plot)
    cbar.ax.set_ylabel("$E_z$ [a.u.]")
    cbar.set_ticks([0])
    
    h5_file.close()
    try:
        eps_file.close()
    except:
        print("Epsilon file couldn't be closed.")
    
    def click_save(b):
        savename= os.path.splitext(os.path.split(fpath)[1])[0]
        print("Save..")
        fig.savefig("img/{0}.png".format(savename))
        print("Saved. Please edit the filename in the image directory.")
            
    button= widgets.Button(
        description='Save image',
        disabled=False,
        button_style='', # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me',
        icon='check'
    )
    display(button)
    button.on_click(click_save)
    
    fig.tight_layout()
    return fig, ax
    