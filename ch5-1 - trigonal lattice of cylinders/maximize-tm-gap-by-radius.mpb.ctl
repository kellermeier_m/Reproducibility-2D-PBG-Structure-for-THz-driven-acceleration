; Max Kellermeier, max.kellermeier@hotmail.de, 2017
(set! num-bands 8)

(define-param EPSILON (* 1.95 1.95))
(define-param K_Z 0.0)  ; Out-of-plane component

(define lattice-constant 1 )  ; reference scale, a
(define Glass (make dielectric (epsilon EPSILON)))

(set! geometry-lattice (make lattice (size lattice-constant lattice-constant no-size)
                         (basis1 (/ (sqrt 3) 2) 0.5)
                         (basis2 (/ (sqrt 3) 2) -0.5)))
(define k_z (/ K_Z lattice-constant)) ; used have the input K_Z  in units of 1/a
(set! k-points (list (vector3 0 0 k_z)          ; Gamma
                     (vector3 0 0.5 k_z )        ; M
                     (vector3 (/ -3) (/ 3) k_z) ; K
                     (vector3 0 0 k_z)))        ; Gamma
(set! k-points (interpolate 9 k-points))
(set! resolution 128)


(define (retrieve-gap-between-bands_1_3)
;  ; TODO: checking for correct parity needs to be done
  (define maximum-band1 (car (cdr ( car band-range-data ))))
  (define minimum-band3 (car (car ( caddr band-range-data ))))
  ; mid-gap-ratio
  (/ (- minimum-band3 maximum-band1) (/ (+ maximum-band1 minimum-band3) 2 )
  )
)

(define (first-tm-gap r)
  (set! geometry (list (make cylinder
                         (center 0 0 0)
                         (radius r)
                         (height infinity)
                         (material Glass)
                         )))
  (run)
  (retrieve-gap-between-bands_1_3 )
)

; 0.01 is the tolerance
(define result (maximize first-tm-gap 0.01 0.1 0.6))
(print "Optimization of radius for k_z = " K_Z "\n")
(print "radius at maximum: " (max-arg result) "\n")
(print "gap size at maximum: " (* (max-val result) 100) "% \n")

