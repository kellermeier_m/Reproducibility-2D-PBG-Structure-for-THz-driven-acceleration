# Chapter 5.1: Band Structure of a Trigonal Lattice of cylinders
A jupyter notebook is provided at the end which holds the plotting snippets.

## In-plane band structure
The script *hexagonal-phc-of-cylinders-bands.mpb.ctl* is used to compute the band structure at a fixed $`k_z`$ value. The original script is located at *MPB/2d-hexagonal/glass-hexagonal-cyl-bands.ctl*. Originally, the script used `(run-tm)` and `(run-te)` to compute the bands based on their polarization, as it is displayed in chapter 5.1. As the modes **do not split** in TE and TM modes anymore for $`k_z \neq 0 `$ the run command is generalized later but the code is kept in comments.

If both `(run-tm)` and `(run-te)` are called consecutively in the script, then both results are listed in the output file.
```sh
mpb RADIUS=0.2 K_Z=0.0 hexagonal-phc-of-cylinders-bands.mpb.ctl | tee hexagonal-phc-of-cylinders-bands.out
```
Extracting the frequencies:
```sh
grep tmfreqs: hexagonal-phc-of-cylinders-bands.out > hexagonal-phc-of-cylinders-bands.tm.dat
grep tefreqs: hexagonal-phc-of-cylinders-bands.out > hexagonal-phc-of-cylinders-bands.te.dat
```

## Maximizing the TM band gap
The first TM band gap is maximized with respect to the radius of the cylinders.
The original script is located at *MPB/2d-hexagonal/optimize-a/hexagonal-optimize-a.ctl*.
```sh
mpb maximize-tm-gap-by-radius.ctl | tee maximize-tm-gap-by-radius.out
```
Only $`k_z=0`$ was conducted. 

## Single Band including out-of-plane propagation
The script *hexagonal-phc-of-cylinders-bands.mpb.ctl* was run with varied values of $`k_z=0`$. The data of the *first* band was extracted from all output files and patched together.
Varying the out-of-plane wave vector and calling the simulation was handled by the following short python snippet which outputs all files into the directory *varied_k_z_output*:
```python
import numpy as np
import os

if __name__ == '__main__':
    dir_path = "varied_k_z_output"
    for k_z in np.linspace(0,1,50, endpoint=False):
        cmd_mpb = "mpb K_Z={0:.3f} RADIUS={1:.3f} glass-hexagonal-cyl-bands.ctl | tee {2}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.out".format(k_z, r, dir_path)
        print("TO BE EXECUTED:", cmd_mpb)
        os.system(cmd_mpb)
        cmd_grep= "grep freq {1}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.out > {1}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.freq.dat".format(k_z, dir_path)
        print("TO BE EXECUTED:", cmd_grep)
        os.system(cmd_grep)
    
    os.chdir(dir_path)
    for file in os.listdir('.'):
        if file.endswith(".freq.dat"):
            # scheme of the files:
            # freqs:, k index, kx, ky, kz, kmag/2pi, band 1, band 2, band 3, band 4, band 5, band 6, band 7, band 8
            mpb_output = np.genfromtxt(file, skip_header=1, delimiter=",", dtype="str")

            for i in range(numBands):
                with open('{0}.band.dat'.format(i+1), "ab") as f:
                    # write kx, ky, kz, freq
                    np.savetxt(f, mpb_output[ :, [2,3,4, 6+i] ], fmt='%s', delimiter=",")
    os.chdir('..')
```

A 3D plot for the first band is created together with its projection to show what the projected band diagram consists of.

## Projected band diagram 
The more important information is the occurance of a band gap at a non-vanishing out-of-plane wavevector $`k_z \neq 0`$. Here, the minimum and maximum frequencies were read from the single band files for each $`k_z`$ separately, e.g. $` \max{\omega_1(k_z=0.1, k_t)} `$ and $` \min{\omega_1(k_z=0.1, k_t)} `$.


## Plots
The figures of the results are shown in *Band structure of trigonal lattice.ipynb* which uses *libanalysis.py* partially.