; Max Kellermeier, max.kellermeier@hotmail.de, 2017
(define-param supercellsize 7)
(set-param! num-bands 50)
                    
(define-param RADIUS 0.24)
(define-param EPSILON (* 1.95 1.95))
(define-param K_Z 0.8)  ; Out-of-plane component
(define-param DEFECTRADIUS (+ RADIUS 1)) ;
(define-param SMALLER_RADIUS 0.0)
(define-param INNER_RADIUS 0.0)

(define lattice-constant 1)  ; reference scale, a

(define Glass (make dielectric (epsilon EPSILON)))

(define sx (* supercellsize lattice-constant))
(set! geometry-lattice (make lattice (size sx sx no-size)
                         (basis1 (/ (sqrt 3) 2) 0.5)
                         (basis2 (/ (sqrt 3) 2) -0.5)))

(set! geometry (list (make cylinder
                       (center 0 0 0)
                       (radius RADIUS)
                       (height infinity)
                       (material Glass)
                       )))
(set! geometry (geometric-objects-lattice-duplicates geometry))

(set! geometry (append geometry
  (list
    (make cylinder
      (center 0 0 0)
      (radius DEFECTRADIUS)
      (height infinity)
      (material air)
    )
    (make cylinder
      (center 0 0 0)
      (radius INNER_RADIUS)
      (height infinity)
      (material Glass)
    )
  )
))


(define geometrySmallRods
  (list 
    (make cylinder
      (center 1 0 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center 0 1) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center 1 -1 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center -1 0 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center 0 -1 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center -1 1 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
  )
)

 (set! geometry (append geometry
  geometrySmallRods
 ))
 
(set-param! resolution 32)

(define-param k-t 0.0)
(set! k-points 
  (list (vector3 k-t k-t K_Z))
)


(define wvgCenter 
    (make cylinder 
      (center 0 0 0)
      (radius DEFECTRADIUS)
      (radius RADIUS)
      (height infinity)
      (material air)
      )
)    

(define (defect-energy-at-band which-band)
  (get-dfield which-band)
  (compute-field-energy)
  (print
   "energy in cylinder at band " which-band " : "
   (compute-energy-in-objects wvgCenter)
   "\n")
)

; (set! filename-prefix (string-append "defect-" (number->string INNER_RADIUS) "-fixed-kz-"  ) )
; (set! filename-prefix (string-append "second-cell-" (number->string SMALLER_RADIUS) "-fixed-kz-"  ) )
; (set! filename-prefix (string-append "defect-cut-" (number->string DEFECTRADIUS) "-fixed-kz-"  ) )

(set! target-freq K_Z)
(run 
  display-zparities display-yparities 
  output-efield-z
  defect-energy-at-band 
  output-poynting-z
)
