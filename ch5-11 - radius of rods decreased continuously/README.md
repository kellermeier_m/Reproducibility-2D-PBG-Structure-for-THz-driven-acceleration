# Radius of Rods decreased continuously
The script runs the simulation at a fixed radius of the center rod and a fixed wavevector. To find the evolution the computation is done for different center rod radii which is realized via the following bash script:
```sh
#!/bin/bash
r=0.24
defect=0.24
for r_D in $(seq 0.00 0.02 0.24)
do    
    MPBCMD="mpb supercellsize=7 RADIUS=$r K_Z=0.8 INNER_RADIUS=${r_D} DEFECTRADIUS=$defect SMALLER_RADIUS=0.0  defect-fixed-kz.mpb.ctl"
	echo "Command to run"
	echo "${MPBCMD}"
	$MPBCMD  | tee var_r_${r}_defect_${defect}_r_D_${r_D}.out
	mpb-data -r -n 32 *.h5
	h5topng -C epsilon.h5:data-new -c RdBu -d z.i-new e.*.h5
	rm e.*.h5
	mkdir var_r_${r}_defect_${defect}_r_D_${r_D}
	mv *.png var_r_${r}_defect_${defect}_r_D_${r_D}
	mv *.out var_r_${r}_defect_${defect}_r_D_${r_D}
	mv *.h5 var_r_${r}_defect_${defect}_r_D_${r_D}
done
```

As a target frequency is used the band labels do not neccessarily be identical for different radii. For example, while band number 40 is the defect dipole mode for $`r_D=0.20`$, the same mode has band number 20 when $`r_D=0.14`$. The field pattern are output during the simulation and converted to an image to manuallycheck which band is the characteristic dipole. The frequencies $`\omega(r_D)`$ were manually read off and plotted together with the known band gap boundaries from ch. 5.1.

Plotting:
```python
#! python
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

lower_band=0.729 # with higher res
upper_band=0.826
single_rod_defect =pd.read_csv("../Simulation-results/MPB/supercell-2d-with-stored-energy/fixed-kz/varied_single_rod_defect_r_0.24_kz_0.8.dat", comment="#")

fig, ax = plt.subplots()
ax.set_title("Evolution of localized mode, $r=0.24, k_z=0.8$")
ax.set_ylabel("fa/c")
ax.set_xlabel(r"Defect radius $r_D/a$")

ax.axhspan(0, lower_band, color='C0')
ax.axhspan(upper_band, 1, color="C0")
ax.plot(single_rod_defect["r_D"].values, single_rod_defect[" freq"].values, color="C1", marker=".", label="dipole", zorder=-1)

ax.set_xlim((0,0.24))
ax.set_ylim((0.70, 0.85))
ax.locator_params(axis='y', nbins=5)
ax.legend()
```
where the file *varied_single_rod_defect_r_0.24_kz_0.8.dat* looks like:
```
# From /gpfs/data/kellermeier_m/MPB/decreased_defect_kz_0.8
## Dipole, band 11, 12, degenerate; monopole, band 20, b40
r_D, energy, freq
0.24, 0.010, 0.729101
0.22, 0.35, 0.741394
## Dipole, band 1, 2, deg; monopole: band 40, energy 6%, in band
0.20, 0.34, 0.762675
## Dipole, band 1, 2, deg; monopole: band 20, in band
0.18, 0.26, 0.78545
## Dipole, band 1, 2, deg; monopole: band 20, in band
0.16, 0.17, 0.805762
## Dipole, band 1, 2, deg; monopole: band 20, in band; all other modes in band
0.14, 0.086, 0.820635 
## Dipole, band 1, 2, deg; monopole: band 20, in band; all other modes in band
0.12, 0.031, 0.829121
```
