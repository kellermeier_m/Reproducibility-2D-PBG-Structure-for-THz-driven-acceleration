# Resonances of the Finite Waveguide
*excite-mounted_2D_small-wvg.meep.ctl* is based on *MEEP/2D-hexagonal-PBG-wvg/excite-mounted_2D_wvg.ctl* while *excite-mounted_2D_large-wvg.meep.ctl* is originally located at *MEEP/2D-PBG-large-wvg/excite-mounted_2D_wvg.ctl*.

To find the resonances:
```sh
meep excite-mode?=false fcen=0.3 df=0.5 excite-mounted_2D_small-wvg.meep.ctl | tee excite-mounted_2D_small-wvg_f-0.3-df-0.5.out
```
and 
```sh
meep excite-mode?=false fcen=0.3 df=0.5 excite-mounted_2D_large-wvg.meep.ctl | tee excite-mounted_2D_large-wvg_f-0.3-df-0.5.out
```
Extracting the resonances
```sh
grep harminv0: excite-mounted_2D_large-wvg_f-0.3-df-0.5.out > harminv.out
```

To get the field pattern the script was run again with a narrow band source, e.g.
```sh
meep excite-mode?=true fcen=0.484 df=0.01 excite-mounted_2D_large-wvg.meep.ctl | tee excite-mounted_2D_large-wvg_f-0.3-df-0.5.out
```
It writes the field $`E_z`$ to a *.h5* file which is plotted using *h5topng* from *h5utils*.


