# Max Kellermeier, max.kellermeier@hotmail.de, 2017
import numpy as np
import h5py  # http://docs.h5py.org/en/latest/quick.html
from scipy import interpolate


class Field_Integrator_on_axis:
    def __init__(self, path, freq, resolution=20):
        '''
        path
            Path to the h5 file containing the field data Ez.
        freq
            Frequency of the simulated field, specified in MEEP units.
        resolution
            Resolution of the grid
        '''
        self.path = path
        self.ez = h5py.File(path).get("ez")
        self.freq = freq
        self.resolution = resolution
        self.dt= 1/freq /resolution  # in units of c/a
        self.x_dim = self.ez.shape[0]
        self.y_dim = self.ez.shape[1]
        self.z_dim = self.ez.shape[2]
        self.t_dim = self.ez.shape[3]
        self.x_len = self.x_dim/resolution  # grid length in meep units 
        self.y_len = self.y_dim/resolution
        self.z_len = self.z_dim/resolution
        #self.t_dim = self.ez.shape[3]
        
        self.ez_by_z_t = self.ez[self.x_dim//2, self.y_dim//2,:,:]  # centered at x=0, y=0
        
    def interpolate_with_time_periodictiy(self):
        '''
        The scipy interpolation function 'interp2d' does not include periodic functions. For a simple function given 
        by a sample of m datapoints the interpolation is done between 
            point 0 and point 1,
            point 1 and point 2,
            ...
            point m-2 and point m-1
        Including periodicity, point m-1 and 0 also have to be interpolated. This is done in this function by extending
        the dataset at the end with the first datapoint. So:
        f[m]=f[0]
        and the interpolated dataset has the shape (m+1, m+1).
        
        Here, the interpolation is done with a two-dim. function E_z(z,t) where the temporal component is treated periodic.
        
        Return:
            f_ez_extended - interpolated function as function object. Keep in mind that input data points will be 
            ordered before evaluation. So evaluation over the interval "t in [0, 4T]" can not just be done with t%T.
            The interval has to be split up in intervals of [0, T), evaluated seperately, and the results concatenated 
            afterwards.
        '''
        # for time periodic interpolation the last point has to be interpolated with the first point. 
        # Therefore both the interpolated E_z field and the timepoints have to be extended
        ez_at_t_0 = self.ez_by_z_t[:,0].reshape( self.z_dim, 1)
        # time coordinates extended by one for the peridicity
        t_coord = np.arange(self.t_dim + 1)   #TODO: directly use the correct time scale
        z_coord = np.linspace(0,self.z_len, self.ez_by_z_t.shape[0], endpoint=False)
        on_axis_ez_extended=np.concatenate((self.ez_by_z_t, ez_at_t_0), axis=1)
        # Now, the dataset E_z(z,t) is extended with the first datapoint E_z(z, t=0) at the end
        
        # finally interpolation
        self.f_ez_extended = interpolate.interp2d(t_coord, z_coord, on_axis_ez_extended)
        return self.f_ez_extended
        
    def periodic_E_z(self, t, z):
        '''
        z, t: 1-D arrays at which the on-axis field E_z(t,z) will be evaluated based on the interpolation. 
        
        It is assumed that the field is periodic in the time component.
        The interpolation was only done in the provided period T. Later points always give the result at t=T. So for times
        t> T the evaluation will be done at 't mod T'. Unfortunatelly, the interpolation routine sorts the input array such 
        that for [0.5T, T, 1.5T, 2T] mod T = [0.5T, T, 0.5T, T] the returned result corresponds to [0.5T, 0.5T, T, T]. This
        is why the interpolation has to be evaluated in separate intervals
        '''
        # TODO-----------------------
        # temporal continued
        # num_periods= 2
        # t_cont = np.linspace(0,num_periods * t_coord.shape[0], num=100)
        if not hasattr(self, 'f_ez_extended'):
            self.interpolate_with_time_periodictiy()
        
        
        t_max= self.f_ez_extended.x_max        
        if t[-1] <= t_max:
            return f_ez_extended(t, z)
        else:
            ez_arr=[]
            split_at = t.searchsorted( np.arange((t[0]//t_max +1)* t_max, np.ceil(t[-1]), t_max)  )
            list_t_sections= np.array_split(t, split_at )
            for section in  list_t_sections:
                ez_arr.append(self.f_ez_extended(section%t_max, z))
            return np.concatenate(ez_arr, axis=1)
        
    def int_over_particle(self, particle, timerange):
        '''
        It is assumed that the particle is on axis and travels along the z-axis. 
        '''
        z_particle = particle.z_t(timerange*self.dt)
        forces_at_all_particle_points= self.periodic_E_z(timerange, z_particle)
        #very inefficient
        return np.sum(np.diagonal(forces_at_all_particle_points))
    
#         split_at = t_cont.searchsorted( np.arange((t_cont[0]//f_ez_extended.x_max +1)* f_ez_extended.x_max, np.ceil(t_extended[-1])+1, f_ez_extended.x_max)  )
#         ez_arr=[]
#         list_t_sections= np.array_split(t_cont, split_at)
#         for section in  list_t_sections:
#             ez_arr.append(f_ez_extended(section%t_coord.shape[0], z_coord))
#         ez_cont = np.concatenate(ez_arr, axis=1)


class OnAxisParticle:
    def __init__(self, initial_z, beta):
        '''
        initial_z: the particle's initial position along the z-axis at time t=0
        beta: on-axis velocity in units of c 
        '''
        self.z_0 = initial_z
        self.beta = beta
        # equation of motion without any force
        # timepoint = t_index * dt
        self.z_t = lambda timepoint: self.z_0 + self.beta*timepoint        
    