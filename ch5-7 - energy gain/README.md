# Energy gain
For both simulations from chapter 5.5 the field component $`E_z(x,y,z,t)`$ was written to a file *ez.h5*.
This was used to compute the energy gain.
- particle moving with fixed velocity, tracked through whole structure
- field integration over multiple temporal periods
- on-axis field 2d interpolated in $`(z,t)`$
- periodically extended along z-axis

An object oriented ansatz is used. The objects are implemented in *lib_energy_gain.py* which provides a class for storing and processing the field and a second class for the tracked particle.  
The jupyter notebook  *Energy Gain Analysis.ipynb* uses the classes and the method `int_over_particle(particle)` to compute the energy gain of a particle with certain velocity. This computation is repeated for several velocities and the energy gain is plotted.



