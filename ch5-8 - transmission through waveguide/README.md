# Transmission simulations
## With respect to reference geometry
The script *transmission-large_wvg-ref-geometry.meep.ctl* is based on *MEEP/ large-wvg-transmission/transmission-large_2D_wvg.ctl* and has to be executed twice, for each geometry separately.  

Setting the parameter `geometry-num=0` uses the waveguide geometry while `geometry-num=1` runs the simulation with the hollow core cylinder. The boolean parameter `transmission?` controls whether the desired Gaussian source is used and the flux is measured (`transmission?=true`), or a continuous source is used for visualization purposes of the excitation (`transmission?=false`).  


To calculate the transmission run
```sh
meep resolution=20 geometry-num=0 NUM_CELL_1=5 large-src?=true transmission?=true fcen=0.47 RADIUS=0.24 DPML=4.0  transmission-large_wvg-ref-geometry.meep.ctl | transmission_cells-5-geom-0.out
```
extract the flux data:
```sh
grep flux1 transmission_cells-5-geom-0.out > transmission_cells-5-geom-0.flux.dat
```
repeat the simulation with the reference geometry:
```sh
meep resolution=20 geometry-num=1 NUM_CELL_1=5 large-src?=true transmission?=true fcen=0.47 RADIUS=0.24 DPML=4.0  transmission-large_wvg-ref-geometry.meep.ctl | transmission_cells-5-geom-1.out
```
extract the data:
```sh
grep flux1 transmission_cells-5-geom-1.out > transmission_cells-5-geom-1.flux.dat
```
and finally you get the relative transmission by dividing the flux data:
```python
#! python
def cell_converter(s):
    try: 
        r= float(s)
    except:
        r= np.NaN
    finally:
        return r
        
wvg_flux_data =  np.genfromtxt("transmission_cells-5-geom-0.flux.dat", delimiter=",",  converters={0: cell_converter, 1: cell_converter,2: cell_converter})[:,1:]
 ref_flux_data = np.genfromtxt("transmission_cells-5-geom-1.flux.dat", delimiter=",", converters={0: cell_converter, 1: cell_converter,2: cell_converter})[:,1:]
 
freqs=wvg_flux_data[:,0]
transmission_spectrum=  wvg_flux_data[:,1]/ref_flux_data[:,1]
```

The transmission simluations were repeated for varied number of cells `NUM_CELL_1` and a different radius. 

## Through box
*transmission-large_wvg-through-box.meep.ctl* for simulating the flux through a box is based on *MEEP/transmission-in-and-out/transmission-large_2D_wvg.ctl*. Three flux planes are defined which form a box which covers the waveguide in the transverse plane. One flux region is defined as the `input-flux` which is the transverse plane close to the source. Four planes parallel to the z-axis are patched together to form the `loss-flux` region. The transverse plane far from the source, which is placed at the exit of the waveguide, is termed `trans` for "transmitted".

Run the simulation and extract the fluxes
```sh
meep resolution=20 NUM_CELL_1=10 nfreq=500 transmission?=true fcen=0.45 df=0.4 RADIUS=0.24 transmission-large_wvg-through-box.meep.ctl | tee transmission-large_wvg-through-box.out
grep flux1:  transmission-large_wvg-through-box.out > transmission-large_wvg-through-box.flux.dat
```
The file *transmission-large_wvg-through-box.flux.dat* contains the fluxes from all three flux regions as columns. To get the flux spectra for transmission and loss, the data from the third column resp. the second  has to be divided by the first column.
