# Resonance in truncated crystal
In analogy to ch 5.3 the effectively 2D structure was excited to study the resonances and their Q-factor. The script *resonances-redesigned-crystal.meep.ctl* is based on 
*MEEP/optimize-Q-redesigned/mode-confinement-2D.ctl*

```sh
 meep resolution=20  time-after-src=6000 NUM_CELL_1=6 fcen=0.52 df=0.3 resonances-redesigned-crystal.meep.ctl |tee resonances-redesigned-cells-6.
out
grep harminv resonances-redesigned-cells-6.out
```
This was repeated for `NUM_CELL_1` varied from 5 to 20 and for a narrow band source with shorter decay time, `time-after-src=6000 df=0.01`. The dominating resonance was extracted from each file as described in the thesis. The resonance frequency and the Q-factor depending on the number of cells were plotted.

