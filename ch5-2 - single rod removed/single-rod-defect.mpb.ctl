; Max Kellermeier, max.kellermeier@hotmail.de, 2017
(define-param supercellsize 5 )
(set! num-bands 75) ; many more needed due to back folding to the first Brillouin zone
                    ; corresponds to band 1, 2 and 3 (3* 5^2 = 50) , since one tm gap appears

(define-param RADIUS 0.245) ; changed radius
(define-param EPSILON (* 1.95 1.95))
(define-param K_Z 0.0)  ; Out-of-plane component

(define lattice-constant 1 )  ; reference scale, a
(define Glass (make dielectric (epsilon EPSILON)))
(define sx (* supercellsize lattice-constant))
(set! geometry-lattice (make lattice (size sx sx no-size)
                         (basis1 (/ (sqrt 3) 2) 0.5)
                         (basis2 (/ (sqrt 3) 2) -0.5)))

(set! geometry (list (make cylinder
                       (center 0 0 0)
                       (radius RADIUS)
                       (height infinity)
                       (material Glass)
                       )))
(set! geometry (geometric-objects-lattice-duplicates geometry))

;remove rod
(set! geometry (append geometry
  (list
    (make cylinder
      (center 0 0 0)
      (radius RADIUS)
      (height infinity)
      (material air)
    )
  )
))

(define k_z (/ K_Z lattice-constant)) ; used have the input K_Z  in units of 1/a

(set! k-points (list (vector3 0.0 0.0 k_z)))
(set-param! resolution 32)

(define (defect-energy-at-band which-band)
  (get-dfield which-band)
  (compute-field-energy)
  (print
   "energy in cylinder at band " which-band " : "
   (compute-energy-in-objects (make cylinder (center 0 0 0)
                                    (radius 1.0) (height infinity)
                                    (material air)))
   "\n")
)


(run defect-energy-at-band display-zparities display-yparities)

(output-efield-z 44)
(get-dfield 44)
(compute-field-energy)
(print
 "energy in cylinder at band 44: "
 (compute-energy-in-objects (make cylinder (center 0 0 0)
                                  (radius 1.0) (height infinity)
                                  (material air)))
 "\n")
