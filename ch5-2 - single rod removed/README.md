# Defect mode of single rod removed
The scipt *single-rod-defect.mpb.ctl* is based on "MPB/supercell-2d-hexagonal/glass-hexagonal-cyl-bands.ctl". A supercell is used to compute the band shifted into the gap.
```sh
mpb single-rod-defect.mpb.ctl | tee single-rod-defect_r_0.245.out
```
The defect mode is identified by looking for the largest energy stored in the hollow core and checked afterwards by plotting the field. The stored energy is printed as `energy in cylinder at band 1 : ....`
Here, band 44 has the largest energy in the core. The simulation is repeated to output the field distribution $`E_z`$.

To plot the field distribution, use *lib_field_plot.py* in the main directory by 
```python
plot_ez_by_h5(r"e.k01.b44.z.h5", dataset="z.r-new")
```
As it uses ipywidgets it is recommended to import the function in a jupyter notebook.

The path to the file has to be adjusted accordingly to the output file from previous simulation.
