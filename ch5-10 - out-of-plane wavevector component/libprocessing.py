# Max Kellermeier, max.kellermeier@hotmail.de, 2017
from __future__ import print_function
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
import os
import regex as re

def load_kz_from_file(path_to_kz_val):
    with open(path_to_kz_val, "r") as f:
        s=f.read()
        kz_arr=np.fromstring(s.replace("\n","")[1:-1], sep=" ")
    return kz_arr
	
def freq_parity_energy_from_out(main_dir, dir_path="varied_k_z"):
    with open(main_dir+"/varied_k_z.freq_zparity_energy.dat", "a") as result_f:
        for k_z in np.linspace(0,5, 50):
            # extract freq
            with open("{2}/{1}/defect-dispersion-varied-k_z-{0:0.3f}.out".format(k_z, dir_path, main_dir), "r" ) as sim_f:
                freqs_lines= [s for s in sim_f.readlines() if re.search("freqs", s)]
                result_f.writelines(freqs_lines)
            #extract zparity
            with open("{2}/{1}/defect-dispersion-varied-k_z-{0:0.3f}.out".format(k_z, dir_path, main_dir), "r" ) as sim_f:
                zparity_lines= [s for s in sim_f.readlines() if re.search("zparity", s)]
                result_f.writelines(zparity_lines)
            # extract energy
            with open("{2}/{1}/defect-dispersion-varied-k_z-{0:0.3f}.out".format(k_z, dir_path, main_dir), "r" ) as sim_f:
                result_f.write("energy:,,,,,")   # only three commas added due to initial comma in loop
                for l in map( lambda s: str.split(s)[-1],
                             [s[:-1] for s in sim_f.readlines() if re.search("energy in", s)]
                            ):
                    result_f.write(", "+ l )
                result_f.write("\n")
				
def freq_parity_energy_from_out_to_csv(path_to_kz_val, main_dir, dir_path="varied_k_z"):
    '''    
    The output is csv file containing the frequency, the parity and the 
        energy stored in the center for each k_z value as rows. The columns label the bands of each simulation.
        
    Parameters:
        path_to_kz_val: Path to the file containing the different kz values according to the simulations. The band structure for each entry was computed 
        where the entry of the array acted as the out-of-plane component of the wave vector.
        main_dir: directory to which the ouput is saved to. 
        dir_path: path to the directory containing the output files from the simulations. The number of files in this folder
        should be 4 x len(kz_array). For each simulation there exists an output file "*.o*", an error file "*.e*" and the 
        same for the parallel outputs
        
    REMARK: Could be done more efficiently
    '''
    kz_array= load_kz_from_file(path_to_kz_val)
    with open(main_dir+"/varied_k_z.freq_zparity_energy.dat", "a") as result_f:
        for k_z in kz_array:
            import glob
            for f in glob.glob('{1}/{2}/*kz-{0:1.3f}.o*'.format(k_z, main_dir, dir_path)):
                # extract freq
                with open(f, "r" ) as sim_f:
                    freqs_lines= [s for s in sim_f.readlines() if re.search("freqs", s)]
                    result_f.writelines(freqs_lines)
                #extract zparity
                with open(f, "r" ) as sim_f:
                    zparity_lines= [ (s[:8]+",,,,"+s[8:]) for s in sim_f.readlines() if re.search("zparity", s)]
                    result_f.writelines(zparity_lines)
                # extract energy
                with open(f, "r" ) as sim_f:
                    result_f.write("energy:,,,,,")   # only three commas added due to initial comma in loop
                    for l in map( lambda s: str.split(s)[-1],
                                 [s[:-1] for s in sim_f.readlines() if re.search("energy in", s)]
                                ):
                        result_f.write(", "+ l )
                    result_f.write("\n")
                    # '''
    return main_dir+"/varied_k_z.freq_zparity_energy.dat"

def export_csv_data_to_h5(fname):
    '''
    The function converts the csv file in a pandas dataframe and stores it as an h5 file.
    
    fname: path to the csv file of the multiindex array on band structure, parity and energy

    Return:
        h5_filepath : path to the output file
        df : dataframe stored in the h5 file
    '''
    file_content_varied_kz = [np.array(line.split(",")) for line in open(fname)]
    band_labels=file_content_varied_kz[0][1:]
    file_str_array = np.array(file_content_varied_kz)
    labels=file_str_array[1:4,0]
    data_from_varied_kz=np.delete(file_str_array[:,1:], slice(0,-1,4), axis=0)
    k_z=data_from_varied_kz[:,3][0::3].astype(float)
    index_df = pd.MultiIndex.from_product( [k_z, labels], names=["k_z","quantity"] )
    df= pd.DataFrame(data_from_varied_kz[:,5:].astype(float),
                    index=index_df, columns=band_labels[5:])
    path_to_output_h5= os.path.splitext(fname)[0]+ ".h5"
    df.to_hdf(path_to_output_h5,"df")
    return path_to_output_h5, df	


	
def rename_job_output_files_with_kz(path_to_sim_files, path_to_kz_val):
    '''
    very inefficient solution.
    Description:
        The output  from Merlin cluster is saved in *.o, *.e, *.po, *.pe files. Their name is not associated with their out-of-plane 
        component kz
    '''
    with open(path_to_kz_val, "r") as f:
        s=f.read()
        kz_arr=np.fromstring(s.replace("\n","")[1:-1], sep=" ")

    job_ids_redundant=[file_name.split(".")[-1][-7:] for file_name in os.listdir(path_to_sim_files) if not file_name.endswith("h5") ]
    job_ids=sorted(list(set(job_ids_redundant)))
    assert(len(job_ids) == len(kz_arr))

    cwd = os.getcwd()
    os.chdir(path_to_sim_files)
    for kz, job_id in zip(kz_arr, job_ids):
        for filename in os.listdir("."):
            if filename.endswith(job_id):
                os.rename(filename, filename.split(".")[0] + "-{0:1.3f}.".format(kz) + filename.split(".")[1])
    os.chdir(cwd)