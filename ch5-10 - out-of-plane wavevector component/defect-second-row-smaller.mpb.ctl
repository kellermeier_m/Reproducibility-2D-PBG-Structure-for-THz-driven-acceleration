; Max Kellermeier, max.kellermeier@hotmail.de, 2017
(define-param supercellsize 5 )
(set! num-bands (* 3 supercellsize supercellsize) ) 
(define-param RADIUS 0.245) 
(define-param EPSILON (* 1.95 1.95))
(define-param K_Z 0.0)
(define-param DEFECTRADIUS RADIUS) ;
(define-param SMALLER_RADIUS 0.1)

(define lattice-constant 1)  ; reference scale, a
(define Glass (make dielectric (epsilon EPSILON)))

define sx (* supercellsize lattice-constant))
(set! geometry-lattice (make lattice (size sx sx no-size)
                         (basis1 (/ (sqrt 3) 2) 0.5)
                         (basis2 (/ (sqrt 3) 2) -0.5)))

(set! geometry (list (make cylinder
                       (center 0 0 0)
                       (radius RADIUS)
                       (height infinity)
                       (material Glass)
                       )))
(set! geometry (geometric-objects-lattice-duplicates geometry))

(set! geometry (append geometry
  (list
    (make cylinder
      (center 0 0 0)
      (radius DEFECTRADIUS)
      (height infinity)
      (material air)
    )
  )
))

(define geometrySmallRods
  (list 
    (make cylinder
      (center 1 0 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center 0 1) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center 1 -1 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center -1 0 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center 0 -1 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center -1 1 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
  )
)

(set! geometry (append geometry
  geometrySmallRods
))

(define k_z (/ K_Z lattice-constant)) ; used have the input K_Z  in units of 1/a

(set! k-points (list (vector3 0.0 0.0 k_z)))
(set! resolution 16)

(define (defect-energy-at-band which-band)
  (get-dfield which-band)
  (compute-field-energy)
  (print
   "energy in cylinder at band " which-band " : "
   (compute-energy-in-objects (make cylinder (center 0 0 0)
                                    (radius 1.0)
                                    (height infinity)
                                    (material air)))
   "\n")
)


(run 
  defect-energy-at-band display-zparities display-yparities )