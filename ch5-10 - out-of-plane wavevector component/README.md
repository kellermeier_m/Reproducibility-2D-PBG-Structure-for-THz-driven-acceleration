# Out-of-plane Wavevector component
The script for finding the defect mode in ch. 5.2 was modified to change continuously the wave vector component $`k_z`$.  All python functions for postprocessing of the results are provided in *libprocessing.py*.  

A python script was used to step over $`k_z`$ an submit a simulation for a given value. Afterwards the datasets were joined together.
```python
#! python
def run_mpb_defect(k_z=0., r=0.245, dir_path='varied_k_z'):
	cmd_mpb = "mpb K_Z={0:.3f} defect-dispersion-varied-kz.mpb.ctl | tee {1}/defect-dispersion-varied-k_z-{0:0.3f}.out".format(k_z, dir_path)
    print("TO BE EXECUTED:", cmd_mpb)
    os.system(cmd_mpb)
```
The function was called by
```python
k_values=np.linspace(0,1,50, endpoint=False)
for kz in k_values:
  run_mpb_defect(k_z= kz, r=0.24)
```

All output files are located in a subfolder *varied_k_z* with the following file content:
```
defect-dispersion-varied-k_z-0.000.out
defect-dispersion-varied-k_z-0.020.out
defect-dispersion-varied-k_z-0.040.out
defect-dispersion-varied-k_z-0.060.out
...
```
The joining of the files is done by the function `freq_parity_energy_from_out(main_dir, dir_path="varied_k_z")` which writes the multidimensional array to a csv file. A second function $`export_csv_data_to_h5(fname)`$ converts the csv file in a structured pandas dataframe as a dataframe is easier to handle for analysis.  
The band *45* was selected as the defect mode based on the energy stored inside. Its dispersion $`\omega(k_z)`$ was plotted on top of the projected band diagram from chapter 5.1.


On the merlin cluster (merlin4) the file name did not contain $`k_z`$ which is why it was neccessary to store it in a separate file, 
```python
with open('defect_varied_kz/kz_out.dat', 'w+') as f:
  f.write(np.array2string(k_values, formatter={'float_kind':lambda x: "%.3f" % x}) )
```
and rename the output files after the computation via $`rename_job_output_files_with_kz(path_to_sim_files, path_to_kz_val)`$. Because of the different file name scheme the function for joining the data was also modified and is provided as $`freq_parity_energy_from_out_to_csv(path_to_kz_val, main_dir, dir_path="varied_k_z")`$.




# Seven rods removed
The original script can be found at *MPB/supercell-2d-with-stored-energy/glass-defect-second-row-smaller.ctl*. *defect-second-row-smaller.mpb.ctl* contains a few more parameters which are intended for the following chapters. By setting the defect radius to 1.25 the core consists of 7 missing rods.

The procedure for the defect dispersion is the same as before in which the script is run several times via the python with different wavevector
```sh
mpb K_Z=0.020 DEFECTRADIUS=1.25 SMALLER_RADIUS=0 supercellsize=7 defect-second-row-smaller.mpb.ctl | tee defect-second-row-smaller-k_z-0.020.out
```
and a postprocessing joins the output from all files.  The bands are plotted on top of the projected bands from ch. 5.1.


Later, the script used a more sophisticated approach in which the variation of $`k_z`$ is done within a single simulation script, generating a single output file. You can find this at the current version of *MPB/supercell-2d-with-stored-energy/glass-defect-second-row-smaller.ctl* (commit: 5801c62639614c2c2a85866dda134ddae701ce04)

