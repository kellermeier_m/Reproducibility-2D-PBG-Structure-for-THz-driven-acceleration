; Max Kellermeier, max.kellermeier@hotmail.de, 2017
(define-param DPML 3.) ; Used as thickness of the Perfectly matched layer
(define-param PAD 2.) ; padding from wall

(define-param RADIUS 0.38)    ; Changed from 0.2 to 0.24 as ideal Radius
(define-param EPSILON (* 1.95 1.95))
(define-param DEFECT 0.97)
(define-param MOUNTING_THICKNESS 0.5) ; longitudinal thickness
(define-param fcen 0.52)
(define-param df 0.1)
(define-param rel-long-constant 5) ; longitudinal constant
(define-param LEN 40)
(define-param total-length? true)
(define-param NUM_CELL_1 3)
(define-param NUM_CELL_LONGITUDINAL 2) ; Minumum 2!
(define-param MOUNTING_RADIUS (+ NUM_CELL_1 1)) ; radius of mounting plate is one cell larger than the rod lattice to make sure all rods are completely covered
(define-param only-source? false)

(define longitudinal-constant 1)
(if total-length?
  (set! longitudinal-constant (/ LEN (- NUM_CELL_LONGITUDINAL 1)))
  (set! longitudinal-constant rel-long-constant  ) ; longitudinal spatial period c, in terms of a
)

(define Glass (make dielectric (epsilon EPSILON)))
(define basis1 (vector3* 1 (vector3 1 0 0) ))
(define basis2 (vector3* 1 ( vector3 0.5 (/ (sqrt 3) 2) 0)) )
(define basis3 (vector3 0 0 longitudinal-constant ))

(define length-z 0)
(if total-length? 
  (set! length-z LEN)
  (set! length-z (* 2 
          (* (+ NUM_CELL_LONGITUDINAL 1) longitudinal-constant)  ; (NUM_CELL_LONGITUDINAL + 1) * longitudinal-constant * 2
  ))
)

(define-param GRID_SIZE_X 
    (* 2 (+ 
        (* NUM_CELL_1 (vector3-x basis1) )
        PAD DPML
        )
    )
)
(define-param GRID_SIZE_Y 
    (* 2 (+ 
        (* NUM_CELL_1 (vector3-y basis2) )
        PAD DPML
        )
    )
)
; Put mounting at the beginning and at the end of the rods
(define-param GRID_SIZE_Z 
    (+ (* 2 PAD) (* 2 DPML ) length-z)
)
; ----------------------------------------------------------------------------
(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)))
(set! default-material air )

(define geometryList
  (geometric-object-duplicates
  basis1 ; shift-vector
  (* -1 NUM_CELL_1) ; min-multiple
  NUM_CELL_1 ; max-multiple
  (make cylinder (center 0 0 0) (radius RADIUS) (height length-z) (material Glass) )
))

; remove part from parallelogram to make it hexagonal
; center 
(define cAirBlock (vector3+ (vector3* NUM_CELL_1 basis1) (vector3* NUM_CELL_1 basis2)))
(define thicknessAirBlock 
    (- (* (vector3-norm (vector3+ basis1 basis2)) NUM_CELL_1)
        (* 2 RADIUS)
    )
)

(set! geometryList (append     
    ; Construct parallelogram of lattice cells
    (geometric-objects-duplicates
        basis2 
        (* -1 NUM_CELL_1)
        NUM_CELL_1 
        geometryList
    )
    ; cut off parallelogram at upper right corner
    (list 
        (make block 
            (center cAirBlock )
            (size thicknessAirBlock infinity infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
        ; cut off parallelogram at lower bottom corner
        (make block 
            (center (vector3* -1 cAirBlock ))
            (size thicknessAirBlock infinity infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )     
    ))
)

; Periodic mounting 
(if total-length?
  (set! geometryList ( append geometryList
    (geometric-object-duplicates
      basis3
      0
      (- NUM_CELL_LONGITUDINAL 1)
      (make cylinder
        (center 0 0 (/ length-z (- 2) ))
        (radius MOUNTING_RADIUS)
        (height MOUNTING_THICKNESS)
        (material Glass)
      )
    )
  ))
)

(set! geometryList ( append geometryList
  (list
    (make cylinder
      (center 0 0 0)
      (radius DEFECT)
      (height infinity)
      (material air)
    )
  ))
)

(if (not only-source?)
  (set! geometry geometryList)
)

; --------------------------------------------------------------------  
(set-param! resolution 20)
(set! pml-layers (list (make pml (thickness DPML))))

(define  dir-name "transmitted-field-"  )
(if (not (= DPML 3.0))
  (set! dir-name (string-append dir-name "-pml-" (number->string DPML)  ) )
)
(if (not (= RADIUS 0.2))
  (set! dir-name (string-append dir-name "-r-" (number->string RADIUS)  ) )
)
(if (not (= NUM_CELL_1 4))
  (set! dir-name (string-append dir-name "-cells-" (number->string NUM_CELL_1)  ) )
)
(set! dir-name (string-append dir-name "-l-" (number->string length-z)  ) )
(set! dir-name (string-append dir-name "-f-" (number->string fcen) "-df-" (number->string df)  ))
(if only-source?
   (set! dir-name (string-append dir-name "-src-out") )
)

(set! dir-name (string-append dir-name "-res-" (number->string resolution) ))
(use-output-directory dir-name)

; (define src-size DEFECT)
(define src-size (* 0.75 2 NUM_CELL_1 (vector3-y basis2) ) )															
(set! sources (list
               (make source
                 (src (make gaussian-src (frequency fcen) (fwidth df)  )              
                  )
                 (component Ez)
                 (center 0 0 (/ length-z -2)  )
                 (size src-size src-size 0)
             )
))

(if only-source?
  (run-until 601
    (in-point
      (vector3 0 0 (/ length-z -2 ) )
      (to-appended "ez-source" (at-every (/ 1 2 resolution) output-efield output-hfield))
    )
  )
                          
  (run-sources+ 
    (stop-when-fields-decayed
       50 Ez
       (vector3 0 0 (- (* 0.5 GRID_SIZE_Z) DPML PAD ) )
       1e-3
    )
    (after-sources+ 200 (harminv Ez (vector3 0 0 (/ length-z 2)) fcen df))
;    (at-beginning output-epsilon)
    (in-point
      (vector3 0 0 (/ length-z 2))
      (to-appended "ez-at-exit-on-axis" (at-every (/ 1 2 resolution) output-efield output-hfield))
    )
  )
)
