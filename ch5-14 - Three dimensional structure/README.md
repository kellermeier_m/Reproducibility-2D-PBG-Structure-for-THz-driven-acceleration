# Three dimensional structure
Original file: *MEEP/transmitted-field-at-point.ctl*
```sh
meep resolution=18 NUM_CELL_1=8 LEN=40 DPML=10 PAD=5 df=0.4 transmitted-field-at-point.meep.ctl | tee transmitted-field-at-point_len-40.out
meep resolution=18 NUM_CELL_1=8 LEN=60 DPML=10 PAD=5 df=0.4 transmitted-field-at-point.meep.ctl | tee transmitted-field-at-point_len-60.out
```
Each simulation outputs a file *ez-at-exit-on-axis.h5* which contains the field at the waveguide exit, $`E_z(0,0,len/2,t)`$. Another run is performed with `only-source?=true` with which the input Gaussian in free space is recorded. For this purpose the run time has to be adjusted to the first simulation
```scheme
(run-until 601
   ...
```
In the end both datasets were used to compute the transfer function and the group delay using python. The script is given in the jupyter notebook *Transmitted pulse at exit.ipynb*
 
 
#### Infinitely long unmounted structure
 The script *transmitted-field-at-point-inf-structure.meep.ctl* is just a copy of the previous one in which the mounting in the geometry was removed and the length of the glass rods is set to infinity. The length parameter is kept to specify the cell size of the computation.  
 The simulation is executed with the same parameters as the previous ones.
 