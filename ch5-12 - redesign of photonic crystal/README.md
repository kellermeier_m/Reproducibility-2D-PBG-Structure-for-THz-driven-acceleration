# Redesign of photonic crystal
*defect-second-row-smaller.mpb.ctl* from ch. 5.10 was used to compute the defect bands in the projected band diagram for the new types of defect.

### Band structure of the defects
For the case of varied rod size of the inner six rods the parameters for the script were set to:
```sh
mpb K_Z=0.500 DEFECTRADIUS=1.25 SMALLER_RADIUS=0.15 supercellsize=7 defect-second-row-smaller.mpb.ctl | tee defect-r2_0.15-k_z-0.500.out
```
The variation of $`k_z`$ was done again via the provided python script as well as the postprocessing. The defect bands were plotted together with the projected band diagram of the perfect crystal.

For the rod cutting defect the size of the inner six rods was set to zero, `SMALLER_RADIUS=0`, as they are added after the air core.
```sh
mpb K_Z=0.500 DEFECTRADIUS=1.0 SMALLER_RADIUS=0 supercellsize=7 defect-second-row-smaller.mpb.ctl | tee defect-rD_1.00-k_z-0.500.out
```

### Evolution studies
The evolution of the defect bands was studied with *defect-fixed-kz.mpb.ctl* from ch. 5.10. This time all determined modes in the band gap were plotted such that the manual filtering of the data points was not needed anymore.  
The parameters for iteration of the simulation were adjusted as follows for the decreasing radius of the inner six rods:
``sh
#!/bin/bash
r=0.24
defect=1.24
for r2 in $(seq 0.00 0.02 0.24)
do    
  MPBCMD="mpb supercellsize=7 RADIUS=$r K_Z=0.8 INNER_RADIUS=0.0 DEFECTRADIUS=${defect} SMALLER_RADIUS=${r2}  defect-fixed-kz.mpb.ctl"
	echo "Command to run"
	echo "${MPBCMD}"
	$MPBCMD  | tee var_r_${r}_defect_${defect}_r2_${r2}.out
	mpb-data -r -n 32 *.h5
	h5topng -C epsilon.h5:data-new -c RdBu -d z.i-new e.*.h5
	rm e.*.h5
	mkdir var_r_${r}_defect_${defect}_r2_${r2}
	mv *.png var_r_${r}_defect_${defect}_r2_${r2}
	mv *.out var_r_${r}_defect_${defect}_r2_${r2}
	mv *.h5 var_r_${r}_defect_${defect}_r2_${r2}
done
```


For the rod cutting defect `SMALLER_RADIUS=0`:
```sh
#!/bin/bash
r=0.24
for defect in $(seq 0.76 0.02 1.24)
do    
  MPBCMD="mpb supercellsize=7 RADIUS=$r K_Z=0.8 INNER_RADIUS=0.0 DEFECTRADIUS=${defect} SMALLER_RADIUS=0.0  defect-fixed-kz.mpb.ctl"
	echo "Command to run"
	echo "${MPBCMD}"
	$MPBCMD  | tee var_r_${r}_defect_${defect}.out
	mpb-data -r -n 32 *.h5
	h5topng -C epsilon.h5:data-new -c RdBu -d z.i-new e.*.h5
	rm e.*.h5
	mkdir var_r_${r}_defect_${defect}
	mv *.png var_r_${r}_defect_${defect}
	mv *.out var_r_${r}_defect_${defect}
	mv *.h5 var_r_${r}_defect_${defect}
done
```


### Narrow band gap
The projected band structure from ch. 5.1 was computed for different rod radii, by passing the parameter `RADIUS` to mpb. From each plot the intersection of the lower gap boundary and the upper boundary with the SOL dispersion was read off. Via a python script the gap size was computed.

The csv file:
```
r, f_lower, f_upper
0.24, 0.685, 0.906
0.2, 0.793, 1.22
0.4, 0.491, 0.503 
0.16, 0.935, 1.410
0.38, 0.508, 0.539
0.39, 0.500, 0.521
0.25, 0.678, 0.890
0.3, 0.596, 0.721
```

```python
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

f_ranges=pd.read_csv("../Simulation-results/MPB/SOL_band_gap_ranges.dat", sep=",", comment="#")
df = pd.DataFrame(index=f_ranges.index, columns=["r", "midgap f", "gap-midgap ratio"])
df["r"]= f_ranges["r"]
df["midgap f"] = (f_ranges[" f_lower"] + f_ranges[" f_upper"])/2
df["gap-midgap ratio"]=( f_ranges[" f_upper"] - f_ranges[" f_lower"] )/df["midgap f"]
df_out=df.loc[:,"midgap f" : "gap-midgap ratio"].copy()
df_out.columns=["midgap fa/c", "gap-midgap ratio [%]"]
df_out.index = df["r"]
df.index.name = "r/a"
df_out["gap-midgap ratio [%]"] = df_out["gap-midgap ratio [%]"]  * 100

print(df_out.sort_index().to_latex(column_format="SSS"))
```

