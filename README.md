# Design of a 2D Photonic Band Gap Structure for THz-driven particle acceleration based on Fused Silica
This repo contains the simulation scripts used in the thesis *Design and Fabrication of a Two-Dimensional Photonic Band Gap Structure for Terahertz-driven particle acceleration based on Fused Silica*  and is based on the repositories [MPB](https://gitlab.psi.ch/kellermeier_m/MPB) and [MEEP](https://gitlab.psi.ch/kellermeier_m/MEEP). While the base repos are oriented towards the utilzed software during this study and more scripts are provided than presented in the study, this repo follows the chronological order in the final thesis.


Two different simulation softwares were applied, the finite-difference time domain package [MEEP](https://meep.readthedocs.io/en/latest/) and the frequeny-domain based [MPB](https://mpb.readthedocs.io/en/latest/). Installation instructions for the PSI cluster are provided in [meep-on-merlin](https://gitlab.psi.ch/kellermeier_m/meep-on-merlin).

# Quick Guide
The directories are named by the chapters in the thesis. As no results were presented in chapters *1* to *3* the first directory starts with *4*.  The scripts are written in [GNU Guile](https://de.wikipedia.org/wiki/GNU_Guile) based *Scheme*. 
- *MEEP* scripts end with `.meep.ctl`
- *MPB* scripts end with `.mpb.ctl`

The computations are started with 
```
meep PARAM=1 script.meep.ctl
```
where `PARAM=1` is an optional parameter defined in the script.

For MPB the command is almost the same
```
mpb PARAM=1 script.mpb.ctl
```

# Output Files
It is suggested to execute the scripts from a different working directory to separate simulation instructions from simulation data. The script has to be provided with it's path, e.g.

```sh
~/simulation-results/ch3: $ mpb resolution=64 ../../ch3/example.mpb.ctl | tee example_bands_res64.out
```
The `tee` command is used to write the simulation output to the standard output and a file at the same time. On the cluster this is not necessary as all outputs are stored in a separate file.


The output is required for the analysis, depending on the task. For instance, MPB* prints the band data to the standard output by default, while field data is stored as an *.h5* file. Flux computations in *MEEP* also print their results on the command line. Optionally, *MEEP* allows to [save the flux spectra](https://meep.readthedocs.io/en/latest/Scheme_User_Interface/#flux-spectra) to a *.h5* file.

To extract the band or flux data you may use 
```sh
grep freqs example_bands_res64.out > example_bands_res64.freqs.out 
```
resp.
```sh
grep flux0 example_flux_res20.out > example_flux_res20.flux.out
```
The exact expressions for *grepping* depend on the simulation of interest.


Field patterns are partially visualized using [h5utils](https://github.com/stevengj/h5utils/blob/master/README.md) and partially using python with [h5py](http://h5py.readthedocs.io/en/latest/) and *matplotlib*.

# Further Remarks
MEEP provides methods to decrease the computation ressources by exploting symmetries. During this thesis the symmetry exploitation was not implemented as the simulations always failed using the symmetry properties.

